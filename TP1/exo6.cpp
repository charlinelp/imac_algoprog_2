#include <iostream>

using namespace std;

struct Noeud{
    int donnee;
    Noeud* suivant;
};

struct Liste{
    Noeud* premier;
    // your code
    
  
};

struct DynaTableau{
    int* donnees;
     // your code
    int size;
    int capacite;

   
};



void initialise(Liste* liste)
{
	liste->premier=NULL;
}

bool est_vide(const Liste* liste)
{
    if (liste->premier==NULL){
        return true;
    }
    else {
        return false;
    } 
}


void ajoute(Liste* liste, int valeur)
{
    Noeud* noeud = (Noeud*)malloc(sizeof(noeud));
    noeud->donnee=valeur;
    noeud->suivant=liste->premier;
    liste->premier=noeud;
}

void affiche(const Liste* liste)
{
    Noeud* noeud = (Noeud*)malloc(sizeof(noeud));
    noeud = liste->premier;
    while (noeud != NULL) {
        cout << noeud->donnee << endl;
        noeud=noeud->suivant;
    }
}

int recupere(const Liste* liste, int n)
{
    Noeud* noeud = (Noeud*)malloc(sizeof(noeud));
    noeud = liste->premier;

    for (int i=0; i<n; i++){
         noeud=noeud->suivant;
    }

    return noeud->donnee;
}

int cherche(const Liste* liste, int valeur)
{
    Noeud* noeud = (Noeud*)malloc(sizeof(noeud));
    noeud = liste->premier;
    int i=0;
    while (noeud != NULL) {
        if (noeud->donnee == valeur){
            return i;
        }
        noeud=noeud->suivant;
        i++;
    }
    return -1;
}

void stocke(Liste* liste, int n, int valeur)
{
    // Noeud* noeud = (Noeud*)malloc(sizeof(noeud));
    Noeud *noeud = liste->premier;

    for (int i=0; i<n; i++){
         noeud=noeud->suivant;
    }
    noeud->donnee=valeur;
}

void ajoute(DynaTableau* tableau, int valeur)
{
    if (tableau->size <  tableau->capacite){
        tableau->donnees[tableau->size] = valeur;
        tableau->size+=1;
    }
    else {
     
        tableau->capacite+=1;
        int* capacite =(int*)realloc(tableau->donnees,tableau->capacite*sizeof(int));
        tableau->donnees=capacite;
        tableau->donnees[tableau->size]= valeur;
        tableau->size+=1;    
    }
}


void initialise(DynaTableau* tableau, int capacite){

    tableau->donnees=(int*)malloc(sizeof(int)*capacite);
    tableau->size = 0;
    tableau->capacite = capacite;
}

bool est_vide(const DynaTableau* liste)
{
   if (liste->size!=0){
        return false;
    }
    else {
        return true;
    } 
}

void affiche(const DynaTableau* tableau)
{
    for (int i=0; i<tableau->size; i++){
            cout << tableau->donnees[i] << endl;
    }
}

int recupere(const DynaTableau* tableau, int n)
{
    return tableau->donnees[n];
}

int cherche(const DynaTableau* tableau, int valeur)
{
    for (int i=0; i<tableau->size; i++){
            if( tableau->donnees[i]==valeur){
                return i;
            }
    }
    return -1;
}

void stocke(DynaTableau* tableau, int n, int valeur)
{
    tableau->donnees[n] = valeur;
}
  

void pousse_file(DynaTableau* tableau, int valeur)
//void pousse_file(Liste* liste, int valeur)
{
    
    if (tableau->size <  tableau->capacite){
        for (int i =tableau->size; i>0; i--){
            tableau->donnees[i] = tableau->donnees[i-1];
        }
        tableau->donnees[0] = valeur;
        tableau->size+=1;
    }
    else {
     
        tableau->capacite += 1;
        int* capacite =(int*)realloc(tableau->donnees,tableau->capacite*sizeof(int));
        tableau->donnees=capacite;
         for (int i =tableau->size; i>0; i--){
            tableau->donnees[i] = tableau->donnees[i-1];
        }
        tableau->donnees[0] = valeur;
        tableau->size += 1;
    }
}

//int retire_file(Liste* liste)
int retire_file(DynaTableau* liste)
{
    int tmp = liste->donnees[liste->size-1];
    liste->size-=1;

    return tmp;
    //return 0;

}

void pousse_pile(DynaTableau* liste, int valeur)
//void pousse_pile(Liste* liste, int valeur)
{
    ajoute(liste, valeur);
    //A voir pour faire l inverse
}

//int retire_pile(Liste* liste)
int retire_pile(DynaTableau* liste)
{

    int tmp = liste->donnees[liste->size-1];
    liste->size-=1;

    return tmp;
    //return 0;

}


int main()
{
    Liste liste;
    initialise(&liste);
    DynaTableau tableau;
    initialise(&tableau, 5);

    if (!est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (!est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    for (int i=1; i<=7; i++) {
        ajoute(&liste, i*7);
        ajoute(&tableau, i*5);
    }

    if (est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    std::cout << "Elements initiaux:" << std::endl;
    std::cout << "Liste" << std::endl;
    affiche(&liste);
    std::cout << "Tableau" << std::endl;
    affiche(&tableau);
    std::cout << std::endl;

    std::cout << "5e valeur de la liste " << recupere(&liste, 4) << std::endl;
    std::cout << "5e valeur du tableau " << recupere(&tableau, 4) << std::endl;

    std::cout << "21 se trouve dans la liste à " << cherche(&liste, 21) << std::endl;
    std::cout << "15 se trouve dans le tableau à " << cherche(&tableau, 15) << std::endl;

    stocke(&liste, 4, 7);
    stocke(&tableau, 4, 7);

    std::cout << "Elements apres stockage de 7:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    //Liste pile;
    DynaTableau pile;
    //Liste file;
    DynaTableau file;

    initialise(&pile,5);
    initialise(&file,5);

    for (int i=1; i<=7; i++) {
        pousse_file(&file, i);
        pousse_pile(&pile, i);
    }

    int compteur = 10;
    while(!est_vide(&file) && compteur > 0)
    {
        std::cout << retire_file(&file) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    compteur = 10;
    while(!est_vide(&pile) && compteur > 0)
    {
        std::cout << retire_pile(&pile) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    return 0;
}

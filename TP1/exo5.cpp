#include "tp1.h"
#include <QApplication>
#include <time.h>

int isMandelbrot(Point z, int n, Point point){
    // recursiv Mandelbrot

    /*retourne vrai si le point appartient à l’ensemble de
    Mandelbrot pour la fonction f(z) → z
    2 +point. Un point appartient à cet ensemble si la suite
    zn est bornée, autrement-dit s’il existe un i < n tel que |zi
    | < 2.*/


    if (n!=0){
        if(z.length()<2){
            //z.length cest le module
            return true;
        }
        else{
            z.x = z.x*z.x-z.y*z.y + point.x;
            z.y = 2*z.y*z.x + point.y;

            return isMandelbrot(z, n-1, point);
        }
    }

    return false;

    // check n

    // check length of z
    // if Mandelbrot, return 1 or n (check the difference)
    // otherwise, process the square of z and recall
    // isMandebrot
    return 0;
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow* w = new MandelbrotWindow(isMandelbrot);
    w->show();

    a.exec();
}




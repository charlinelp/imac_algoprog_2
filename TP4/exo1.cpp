#include "tp4.h"
#include "mainwindow.h"

#include <QApplication>
#include <time.h>
#include <stdio.h>

MainWindow* w = nullptr;
using std::size_t;
using std::string;

int Heap::leftChild(int nodeIndex)
{
    //retourne l'index du fils gauche du noeud à l'indice nodeIndex

    return nodeIndex*2+1;
}

int Heap::rightChild(int nodeIndex)
{
    //droit
    return nodeIndex*2+2;
}

void Heap::insertHeapNode(int heapSize, int value)
{
	// use (*this)[i] or this->get(i) to get a value at index i
	int i = heapSize;
    this->get(i) = value;

    while (i>0 && this->get(i)>this->get((i-1)/2)){
        swap(this->get(i), this->get((i-1)/2));
        i = (i-1)/2;
    }
}

void Heap::heapify(int heapSize, int nodeIndex)
{
	// use (*this)[i] or this->get(i) to get a value at index i

    int largest = nodeIndex;
    int indexRight = rightChild(nodeIndex);
    int indexLeft = leftChild(nodeIndex);

    if (indexRight<heapSize){
        if (this->get(rightChild(nodeIndex))> this->get(largest)){
            largest = rightChild(nodeIndex);
        }
    }
    if (indexLeft<heapSize){
        if (this->get(leftChild(nodeIndex))> this->get(largest)){
            largest = leftChild(nodeIndex);
        }
    }

    if (largest != nodeIndex){
        swap(nodeIndex , largest);
        heapify(heapSize, largest);
    }
}

void Heap::buildHeap(Array& numbers)
{
    int size = numbers.size();
    for (int i=0; i<size ; i++){
        insertHeapNode(i, numbers[i]);
    }
}

void Heap::heapSort()
{
    int size = this->size();
        for(int i=size-1; i>0; i-- ){
            swap(0,i);
            this->heapify(i,0);
        }
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    MainWindow::instruction_duration = 50;
    w = new HeapWindow();
	w->show();

	return a.exec();
}
